call plug#begin('~/Documents/.vim/plugged')

Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'machakann/vim-highlightedyank'

Plug 'vim-airline/vim-airline'

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

Plug 'hashivim/vim-terraform'

call plug#end()
