" Picking the python version {{{
function! SetPythonPath()
    if executable("poetry")
        let poetry_env = system('poetry env info -p')
        if v:shell_error == 0
            let g:python3_host_prog = trim(poetry_env) . "/bin/python3"
        endif
    elseif executable("pyenv")
        let pyenv_path = system("pyenv which python")
        if v:shell_error == 0
            let g:python3_host_prog = pyenv_path
        endif
    endif
endfunction
call SetPythonPath()
" }}}

" Indentation and Tab {{{
" docs {{{
" `tabstop`: This sets the number of spaces that <tab> uses. So if `expandtab`
"            is on, then `tabstop` number of spaces are inserted. If it's not
"            then vim indents the line `tabstop` spaces.
" `shiftwidth`: This governs how many spaces makes one level of indentation.
"               This is different than `tabstop` because `tabstop` is only about
"               what a literal tab character is replaced with. `shiftwidth`
"               concerns itself with all the indentation related stuff thats not
"               a tab. Like when you highlight code then >.
" `expandtab`: If true, replace tab characters with `tabstop` spaces.
" `smartindent`: Tries to be a bit more programmy friendly about indentation.
"
" resources:
" - help smartindent
" - help tabstop
" - https://linuxdigest.com/howto/how-to-autoindent-in-vim/
" - https://www.reddit.com/r/vim/comments/99ylz8/confused_about_the_difference_between_tabstop_and/
" }}}
set tabstop=4
set shiftwidth=4
set expandtab
set smartindent
"}}}

" Line numbers {{{
set number
"}}}

" Preview window {{{
" docs {{{
" This generally concerns what happens when a new window is made causing a
" split. This happens when checking documentation and using the `help whatever`
" in vim.
"
" `previewheight`: The height of the new window.
" `splitbelow`: I want the window to come from the bottom as opposed to the left
"               or right or top.
" }}}
set previewheight=20
set splitbelow
"}}}

" System copy and pase {{{
if has('nvim')
    set clipboard+=unnamedplus
endif
"}}}

" Wrap and Formatting {{{
" docs {{{
" `textwidth`: If you're writing and you go more than `textwidth` then wrap
"              according to `formatoptions`.
" `formatoptions`: This has a bunch of on off switches to control exactly how
"                  (mostly text) gets formatted.
"  - `t`: Wrap text (like non code, non comments)
"  - `c`: Wrap comments
"  - `r`: Add a "comment leader" (//, #, etc) on <Enter> in insert mode.
"  - `q`: Wrap comments with `gq` command.
"  - `n`: Vim should be able to recognize lists (via `formatlistpat`) and will
"         format these appropriatly.
" `formatlistpat`: This determines whats a list. This is a regex that has to
"                  match the pre-amble to the text. So in a normal numbered "1.
"                  blah\n2. blah" it has to match "1. " and "2. " and if there's
"                  a sublist it also has to match "  1. ". Mine matches "1.",
"                  "1:", "1)", or "1 " as well as "-", and "*".
" resources:
" - help fo-table
" - help formatlistpat
" - http://blog.ezyang.com/2010/03/vim-textwidth/
" }}}
set textwidth=80
set formatoptions+=t,c,q,n,r
set formatlistpat="^\s*\(\d\+[:.) ]\|-\|*\)\s*"
"}}}

" Color scheme and stuff {{{
" docs {{{
" `colorcolumn`: Highlight this column with ColorColumn
" `highlight ColorColumn`: `ColorColumn` is a "highlight group". This sets it.
" `background`: The colors "depend on the value of 'background'".
"
" The way colors work is you have:
"   gui/cterm: This is for underline, bold, etc.
"   guifg/ctermfg: The foreground color (text color)
"   guibg/ctermbg: The background color
" I think if you use the cterm flavor you only get 256 colors and if you use the
" gui flavor you can get RGB but idk how to activate it or anything.
" }}}
set colorcolumn=81
highlight ColorColumn ctermbg=DarkGrey
set background=dark
colorscheme eldar
"}}}

" Highlighting {{{
" doc {{{
" This will dynamically highlight replacement stuff (ie s///g). It is *still*
" neovim only though.
" }}}
if has('nvim')
    set inccommand=nosplit
endif
"}}}

" Undo {{{
set undofile
set undodir=~/Documents/.vim/undodir
"}}}

" Line Remembrance {{{
" docs {{{
" Remember the line I was on when I exited. So this executes every time a buffer
" opens.
" Note: The '"' mark (that's one double quote) is a special mark that is the
" position where the current buffer was last exited.
" Note: The `line` function will get the line number. In particular you can use
"       `'x` as an argument where `x` is a mark and you can use `$` to get the
"       last line in the current buffer.
"                                                             ╭──────────────────╮
"               ╭───────────────────────────────────────────╮ │ Jump to mark     │
"               │ This gets the line of the `"` mark or the │ │ without changing ├╮
"               │ line where the buffer was last exited.    │ │ the jump list    ││
"               ╰──────────┬──────────────────┬─────────────╯ ╰──────────────────╯│
"                     ─────┴─────        ─────┴─────                             ─┴──
" au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
" ────────┬─────── ─────────────────────────┬────────────────────── ─────┬──────────── ───┬───
" ╭───────┴────────────╮                    ├────────────────────────────│────────────────╯
" │ Executes this when │ ╭──────────────────┴─────────────────╮ ╭────────┴──────╮
" │ any buffer opens.  │ │ The file could have been truncated │ │ Jump to the   │
" ╰────────────────────╯ │ since laste opened; make sure the  │ │ last position │
"                        │ last position is still in bounds.  │ ╰───────────────╯
"                        ╰────────────────────────────────────╯
" references:
" - https://vim.fandom.com/wiki/Using_marks
" - https://stackoverflow.com/questions/45086981/what-are-the-vim-commands-that-start-with-g
" }}}
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
"}}}

" Use jk as escape {{{
inoremap jk <Esc>
"}}}

" Finding words {{{
" docs {{{
" `<CR> -> :noh<CR><CR>`: This gets rid of highlighting by pressing return.
" `n/N -> n/Nzz`: This centers the word that is found when jumping from word to
"                 word.
" }}}
nnoremap <silent> <CR> :noh<CR><CR>
nnoremap n nzz
nnoremap N Nzz
"}}}

" Spelling {{{
" docs {{{
" This is a setup for spell checking. It overrides some maps for ease of
" spellchecking. It's not really meant to be used while doing anything other
" than typing prose.
"
" `zz`: This opens the spell suggestion list and pre-enters 1 to select the
"       first spelling suggestion. However, it actually select it.
" `zn`: Go to the next misspelling.
" `zl`: Go the last misspelling
" `zi`: Add to dictionary
" }}}
nnoremap <leader>ss :call Toggle_Spelling()<CR>
function Toggle_Spelling()
    setlocal spelllang=en_us
    set spell!
    if &spell
        nnoremap zz z=1
        nnoremap zn ]s
        nnoremap zl [s
        nnoremap zi zg
        echo "spell"
    else
        unmap zz
        unmap zn
        unmap zl
        unmap zi
        echo "nospell"
    endif
endfunction
"}}}

" Folding {{{
" docs {{{
" Default foldmethod should be indent as it's the most good all around
" `foldlevel=99`: This should make sure all folds are open when I open the file
" }}}
set foldmethod=indent
set foldlevel=99
"}}}

" Filetype stuff {{{
" docs {{{
" `filetype plugin indent on`: Turn on filetype detection, autoloading filetype
"                              specific vim files, and filetype specific
"                              indentation.
" resources:
" - https://vi.stackexchange.com/questions/10124/what-is-the-difference-between-filetype-plugin-indent-on-and-filetype-indent
" }}}
filetype plugin indent on
au BufRead,BufNewFile *.cppm,*.hm set filetype=cpp
"}}}

